/* Action Creators */

export function inputValue(row, col, val) {
	return {
		type: 'INPUT_VALUE',
		row,
		col,
		val
	};
}

