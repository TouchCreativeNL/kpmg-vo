import React from 'react';

const Crackthecode = () => {
    
    return(
        <div className='crackthecode'>
            <div className='crackthecode__hint'>
                <div class='crackthecode__hint__numbers'>
                    <div className='crackthecode__hint__numbers__number'>6</div>
                    <div className='crackthecode__hint__numbers__number'>1</div>
                    <div className='crackthecode__hint__numbers__number'>4</div>
                </div>
                <div className='crackthecode__hint__text'>
                    One number is correct but wrong placed
                </div>
            </div>

            <div className='crackthecode__hint'>
                <div class='crackthecode__hint__numbers'>
                    <div className='crackthecode__hint__numbers__number'>3</div>
                    <div className='crackthecode__hint__numbers__number'>8</div>
                    <div className='crackthecode__hint__numbers__number'>7</div>
                </div>
                <div className='crackthecode__hint__text'>
                    One number is correct and well placed
                </div>
            </div>

            <div className='crackthecode__hint'>
                <div class='crackthecode__hint__numbers'>
                    <div className='crackthecode__hint__numbers__number'>7</div>
                    <div className='crackthecode__hint__numbers__number'>4</div>
                    <div className='crackthecode__hint__numbers__number'>8</div>
                </div>
                <div className='crackthecode__hint__text'>
                    Nothing is correct
                </div>
            </div>

            <div className='crackthecode__hint'>
                <div class='crackthecode__hint__numbers'>
                    <div className='crackthecode__hint__numbers__number'>2</div>
                    <div className='crackthecode__hint__numbers__number'>1</div>
                    <div className='crackthecode__hint__numbers__number'>0</div>
                </div>
                <div className='crackthecode__hint__text'>
                   Two numbers are correct but wrong placed
                </div>
            </div>

            <div className='crackthecode__hint'>
                <div class='crackthecode__hint__numbers'>
                    <div className='crackthecode__hint__numbers__number'>7</div>
                    <div className='crackthecode__hint__numbers__number'>8</div>
                    <div className='crackthecode__hint__numbers__number'>2</div>
                </div>
                <div className='crackthecode__hint__text'>
                    One number is correct but wrong placed
                </div>
            </div>

        </div>
        
    )
}

export default Crackthecode;