import React from 'react';
import {inputValue} from "../actions/grid";

const colorBoxes = {
    '0': '#4e2379',
    '30': '#00a3a1',
    '60': '#0091da',
    '3': '#0091da',
    '33': '#4e2379',
    '63': '#00a3a1',
    '6': '#00a3a1',
    '36': '#0091da',
    '66': '#4e2379',
};

const getBoxColor = (row, col) => {
    let rowGroup = row - (row % 3);
    let colGroup = (col - (col % 3)) * 10;
    return colorBoxes[rowGroup + colGroup];
};

class Box extends React.Component {
    constructor(){
        super();
        this.state = {value: ''};
        this.onChange = this.onChange.bind(this)
    }

    onChange(e){
        const re = /^[0-9\b]+$/;
        if (e.target.value === '' || re.test(e.target.value)) {
            this.setState({value: e.target.value})
        }
    }

    render() {
        const {row, col, val} = this.props;
        let input;
        switch (val) {
            case "*":
                input = (
                    <div className="blok" style={{backgroundColor: '#0091da'}}>
                        <input
                            className="input_fields"
                            ref='input'
                            value={this.state.value}
                            onChange={this.onChange}
                            maxLength="1"
                            style={{
                                borderRadius: '50px',
                                border: '2px solid #FFF',
                                backgroundColor: 'transparent'
                            }}
                        />
                    </div>
                );
                break;
            case "**":
                input = (
                    <div className="blok" style={{backgroundColor: '#4e2379'}}>
                        <input
                            className="input_fields"
                            ref='input'
                            maxLength="1"
                            value={this.state.value}
                            onChange={this.onChange}
                            style={{
                                borderRadius: '50px',
                                border: '2px solid #FFF',
                                backgroundColor: 'transparent'
                            }}
                        />
                    </div>
                );
                break;
            case "***":
                input = (
                    <div className="blok" style={{backgroundColor: '#00a3a1'}}>
                        <input
                            className="input_fields"
                            ref='input'
                            maxLength="1"
                            value={this.state.value}
                            onChange={this.onChange}
                            style={{
                                borderRadius: '50px',
                                border: '2px solid #FFF',
                                backgroundColor: 'transparent'
                            }}
                        />
                    </div>
                );
                break;
            default:
                input = (
                    <input
                        className="input_fields"
                        ref='input'
                        style={{backgroundColor: getBoxColor(row, col)}}
                        value={val ? val : this.state.value}
                        maxLength="1"
                        onChange={this.onChange}
                    />
                );
        }
        return (
            <td>
                {
                    input
                }
            </td>
        );
    }
}

export default Box;
